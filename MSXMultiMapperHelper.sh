#!/bin/bash

#    MSX Multi Mapper Helper  (c) 2020  Žarko Živanov
#    E-mail: zzarko at gmail

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# directory names
WORKDSK="WorkDSK"
INITIALDSK="InitialDSK"
ROMS="ROMs"
ROMDETECT="ROMs.txt"

# program for making a DSK image by Arnold Metselaar. Source can be downloaded from (Disk management tools):
# https://fms.komkon.org/fMSX/#Downloads
WRDSK='./wrdsk'

# location of BASIC menu
AUTORUN="$WORKDSK"/AUTORUN.BAS

# name of generated image
DSK="out.dsk"

# clear previous files and make initial ones
rm -f "$WORKDSK"/* 2>/dev/null
cp "$INITIALDSK"/*.* "$WORKDSK"/
rm "$DSK" 2>/dev/null

# BASIC menu, first line
MENU_BEGIN='10 CLS:PRINT "Select ROM image to flash:":PRINT'
# BASIC menu, printing of one option
MENU_ITEM_PRINT='LLL PRINT "NNN - RRR"'
# BASIC menu, user input and checking
MENU_CHOICE='100 PRINT:PRINT "YOUR CHOICE:";:INPUT N
110 IF (N<0) OR (N>MMM) GOTO 100
120 ON N GOTO GGG'
# BASIC menu, DOS command for selected ROM
MENU_ITEM_SELECT='LLL A$="CCC":GOTO 1100'
# BASIC menu, copying of DOS command in keyboard buffer and running it, taken from:
# http://www.ricbit.com/mundobizarro/dicas.php
MENU_END='1100 VDP(1)=VDP(1)AND&HDF:DEFUSR=&H156:A=USR(0)
1110 B=65536!+&HFBF0:A$=A$+CHR$(13):FORI=1TOLEN(A$):POKEB+I-1,ASC(MID$(A$,I,1)):NEXT
1120 A=&HF3F8:POKEA+3,&HFB:POKEA+2,&HF0:B=B+LEN(A$)
1130 C=INT(B/256):POKEA+1,C:POKEA,B-C*256:VDP(1)=VDP(1)OR&H20:CALLSYSTEM'

#save keyboard stdin in descriptor 10
exec 10<&0

ans=""
function readans() {
    if [ "$1" == "" ]; then
        echo -n -e "Press Enter or Space ... "
    else
        echo -n -e "$1 [y/n]? "
    fi
    while [ true ]; do
        read -s -t 1 -n 10000 discard  # flush input before reading
        read -s -u 10 -n 1 ans
        special=0
        case $ans in
            $'\e')  ans="n";;
            $'')    ans="y";;
            *)      echo -n "$ans";;
        esac
        if [ "$ans" == "y" ] || [ "$ans" == "Y" ] || [ "$ans" == ' ' ] || [ "$ans" == '\n' ]; then
            ans="y"
            echo ""
            return 1
        fi
        if [ "$ans" == "n" ] || [ "$ans" == "N" ] || [ "$ans" == '\e' ]; then
            ans="n"
            echo ""
            return 0
        fi
    done
}

fl=""
function readfl() {
    echo -n "Which FL program should be used: 1-FL, 2-FL16, 3-FL8, 4-FLK4 ?"
    while [ true ]; do
        read -s -t 1 -n 10000 discard  # flush input before reading
        read -s -u 10 -n 1 ans
        if [ "$ans" -eq "1" ] || [ "$ans" -eq "2" ] || [ "$ans" -eq "3" ] || [ "$ans" -eq "4" ]; then
            break
        fi
    done
    if [ "$ans" -eq "1" ]; then fl="FL";
    elif [ "$ans" -eq "2" ]; then fl="FL16"
    elif [ "$ans" -eq "3" ]; then fl="FL8"
    else fl="FLK4"; fi
}

# add start line to menu code
echo "$MENU_BEGIN" > "$AUTORUN"
println=20      # current line of menu option print
gotoln=1000     # current line of menu oprion command
itemno=1        # current menu item
itemmax=0       # current number of menu items
ongoto=""       # current ON GOTO line numbers
fl1=0           # should FL.COM be copied
fl2=0           # should FL16.COM be copied
fl3=0           # should FL8.COM be copied
fl4=0           # should FLK4.COM be copied
# array for ON GOTO lines
declare -a menu_item_select
# go through all ROM files in ROMS direcrory
echo -e "\nCollecting ROM files..."
for rom in "$ROMS"/*.[rR][oO][mM]; do
    # get ROM name
    rom=$(basename "$rom")
    # extract just the name
    rname=${rom%%(*}
    # trim name
    rname=$(echo "$rname" | xargs)

    # try to determine FL utility for the ROM
    echo -e "\nROM: $rom"
    IFS_backup=$IFS
    IFS=$'\n'
    detect=($(grep -i "$rname" "$ROMDETECT"))
    IFS=$IFS_backup
    if [ ${#detect[@]} -gt 0 ]; then
        echo -e -n "Detected FL program(s):"
        #for name in "${detect[@]}"; do
        for ((i = 0; i < ${#detect[@]}; i++)); do
            name="${detect[$i]}"
            fl="${name##*,}"
            echo -n " $fl"
        done
        echo ""
        if [ ${#detect[@]} -eq 1 ]; then
            readans "Is that correct"
            if [ "$ans" != "y" ]; then
                readfl
                echo ""
            fi
        else
            readfl
            echo ""
        fi
    else
        readfl
        echo ""
    fi

    if [ "$fl" == "FL" ]; then fl1=1; fi
    if [ "$fl" == "FL16" ]; then fl2=1; fi
    if [ "$fl" == "FL8" ]; then fl3=1; fi
    if [ "$fl" == "FLK4" ]; then fl4=1; fi

    # make name without spaces
    rfname="${rname// /}"
    # take first 8 characters
    rfname="${rfname:0:8}"
    # make upprecase
    rfname="${rfname^^}"
    cp "$ROMS/$rom" "$WORKDSK/$rfname.ROM"
    # create PRINT line in menu code for menu item
    item="$MENU_ITEM_PRINT"
    item="${item/LLL/$println}"
    item="${item/NNN/$itemno}"
    item="${item/RRR/$rname}"
    echo "$item" >> "$AUTORUN"
    # add line number for ON GOTO
    if [ "$ongoto" == "" ]; then
        ongoto="$gotoln"
    else
        ongoto="$ongoto,$gotoln"
    fi
    # create command for menu item
    item="$MENU_ITEM_SELECT"
    item="${item/LLL/$gotoln}"
    item="${item/CCC/$fl $rfname.ROM}"
    menu_item_select[$itemno]="$item"
    # increase counters
    println=$((println + 10))
    gotoln=$((gotoln + 10))
    itemno=$((itemno + 1))
    itemmax=$((itemmax + 1))
    itemgoto=$((itemgoto + 1))
done

# add user input and ON GOTO line numbers to menu code
item="$MENU_CHOICE"
item="${item/MMM/$itemmax}"
item="${item/GGG/$ongoto}"
echo "$item" >> "$AUTORUN"

# add commands for ROM files to menu code
for ((i=1; i<=$itemmax; i++)); do
    echo "${menu_item_select[$i]}" >> "$AUTORUN"
done

# add DOS run to menu code
echo -e "\nFinishing autorun menu..."
echo "$MENU_END" >> "$AUTORUN"
unix2dos "$AUTORUN"

# copy needed FL programs
if [ $fl1 -eq 1 ]; then
    cp "$INITIALDSK"/FL/FL.COM "$WORKDSK"/
fi
if [ $fl2 -eq 1 ]; then
    cp "$INITIALDSK"/FL/FL16.COM "$WORKDSK"/
fi
if [ $fl3 -eq 1 ]; then
    cp "$INITIALDSK"/FL/FL8.COM "$WORKDSK"/
fi
if [ $fl4 -eq 1 ]; then
    cp "$INITIALDSK"/FL/FLK4.COM "$WORKDSK"/
fi

# assemble DSK image
echo -e "\nAssembling DSK image..."
$WRDSK "$DSK" "$WORKDSK"/*


# MSX Multi Mapper Helper #

MSX Multi Mapper Helper is a BASH script that creates MSX DSK images with ROM files and autostart menu that enables easy programming of Multi Mapper FLASH cartridges.

Program is targeted at MSX machines with Gotek drive and some of FL supported FLASH Multi Mapper cartridges.

### Prerequisites ###

* wrdsk from Disk management tools by Arnold Metselaar [fMSX](https://fms.komkon.org/fMSX/#Downloads)
    * put the executable in the same place as MSXMultiMapperHelper.sh
* FL v1.32 package by GDX [FL](https://github.com/gdx-msx/FL/)
    * put all COM files in "InitialDSK/FL/" directory
* COMMAND.COM and MSXDOS.SYS
    * put them in "InitialDSK/" directory

### Usage ###

* Place your ROM files inside "ROMs" directory
* Run MSXMultiMapperHelper.sh from terminal
* Answer to questions

![Running from command line](Images/MSX1.png)

* Transfer generated DSK file to MSX and program FLASH cartridge

![MSX menu](Images/MSX2.png)
![Running FL tool](Images/MSX3.png)

### TODO ###

* make command line parameters instead of script variables

### Licence ###

ROMs file is made from docs in FL package.

MSXMultiMapperHelper.sh is licensed under GPL 3.0 or later licence.

### History ###

* 1.0 - Initial version
